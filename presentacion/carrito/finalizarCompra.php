<?php
    date_default_timezone_set('America/Bogota');
    setlocale(LC_ALL, 'es_ES');
    $fecha = date('y/m/d');
    $hora = date("H:i");

    if (isset($_REQUEST["domicilio"])) {//si el cliente quiere entrega a domicilio activa el check
        
        //traigo datos del carro
        $carrito = new carrito();
        $proCarro = $carrito -> listaCarrito($_SESSION["id"]);//lista del carro
        //busco las ordenes anteriores
        $orden = new Orden();
        $ordenExiste = $orden -> ultimaOrden($_SESSION["id"]);//busco si hay  o no hay compras anteriores
        
        if ($ordenExiste != NULL) {
            //ingreso datos de factura con entrega domicilio
            $entrega = "Domicilio";
            $facturar = new Factura("", $_SESSION["id"], $fecha, $hora, $entrega, $ordenExiste+1, "");
            $facturar -> crearFactura();
        } else {
            //ingreso datos de factura con entrega domicilio
            $entrega = "Domicilio";
            $facturar = new Factura("", $_SESSION["id"], $fecha, $hora, $entrega, 1, "");
            $facturar -> crearFactura();
        }
        
        //valido que existan productos en el carro y que tenga ordenes anteriores
        if ($proCarro != NULL && $ordenExiste != NULL) {//valido que si hay productos
            $suma = 0;            
            foreach ($proCarro as $proActual) { //recorro con un foreach
                $datoProducto = new Producto($proActual -> getIdProducto());//traigo todos los datos del producto por el id
                $datoProducto -> consultar();//consulta el producto
                
                $suma = $proActual -> getCantidad() * $datoProducto -> getValorUnidad();//suma para subtotal
                
                //envio los valores de los productos a la Base de Datos
                $ordenes = new Orden("", $proActual -> getIdProducto(), $_SESSION["id"], $proActual -> getCantidad(), $datoProducto -> getValorUnidad(), $suma, $ordenExiste+1);
                $ordenes -> ingresarOrden();
                
                //elimino los datos del carrito Y lo vacio en la Base de Datos
                $carrito = new carrito("", $proActual -> getIdProducto(), $_SESSION["id"], "");
                $carrito->eliminar();
            }
        }else {//si no existen ordenes anteriores entonces se crea la primera orden o compra del cliente
            $suma = 0;
            foreach ($proCarro as $proActual) { //recorro con un foreach
                $datoProducto = new Producto($proActual -> getIdProducto());//traigo todos los datos del producto por el id
                $datoProducto -> consultar();//consulta el producto
                
                $suma = $proActual -> getCantidad() * $datoProducto -> getValorUnidad();//suma para subtotal
                
                $ordenes = new Orden("", $proActual -> getIdProducto(), $_SESSION["id"], $proActual -> getCantidad(), $datoProducto -> getValorUnidad(), $suma, 1);
                $ordenes -> ingresarOrden();
                
                $carrito = new carrito("", $proActual -> getIdProducto(), $_SESSION["id"], "");
                $carrito->eliminar();
            }
        }
        
    } else { // si no activa el check
                
        $carrito = new carrito();
        $proCarro = $carrito -> listaCarrito($_SESSION["id"]);//lista productos del carro
        
        $orden = new Orden();
        $ordenExiste = $orden -> ultimaOrden($_SESSION["id"]);//busco si hay  o no hay compras anteriores
        
        //ingreso datos a factura
        if ($ordenExiste != NULL) {
            //ingreso datos de factura con entrega tienda
            $entrega = "Domicilio";
            $facturar = new Factura("", $_SESSION["id"], $fecha, $hora, $entrega, $ordenExiste+1, "");
            $facturar -> crearFactura();
        } else {
            //ingreso datos de factura con entrega domicilio
            $entrega = "Domicilio";
            $facturar = new Factura("", $_SESSION["id"], $fecha, $hora, $entrega, 1, "");
            $facturar -> crearFactura();
        }

        //ingreso datos a orden
        if ($proCarro != NULL && $ordenExiste != NULL) {//valido que si hay productos y tenga conpras u ordenes anteriores
            $suma = 0;
            foreach ($proCarro as $proActual) { //recorro con un foreach
                $datoProducto = new Producto($proActual -> getIdProducto());//traigo todos los datos del producto por el id
                $datoProducto -> consultar();//consulta el producto
                
                $suma = $proActual -> getCantidad() * $datoProducto -> getValorUnidad();//suma para subtotal
                
                $ordenes = new Orden("", $proActual -> getIdProducto(), $_SESSION["id"], $proActual -> getCantidad(), $datoProducto -> getValorUnidad(), $suma, $ordenExiste+1);
                $ordenes -> ingresarOrden();
                
                $carrito = new carrito("", $proActual -> getIdProducto(), $_SESSION["id"], "");
                $carrito->eliminar();
            }
        }else { //si hay productos pero no ordenes entonces crea la primera orden del cliente 
            $suma = 0;
            foreach ($proCarro as $proActual) { //recorro con un foreach
                $datoProducto = new Producto($proActual -> getIdProducto());//traigo todos los datos del producto por el id
                $datoProducto -> consultar();//consulta el producto
                
                $suma = $proActual -> getCantidad() * $datoProducto -> getValorUnidad();//suma para subtotal
                
                $ordenes = new Orden("", $proActual -> getIdProducto(), $_SESSION["id"], $proActual -> getCantidad(), $datoProducto -> getValorUnidad(), $suma, 1);
                $ordenes -> ingresarOrden();
                
                $carrito = new carrito("", $proActual -> getIdProducto(), $_SESSION["id"], "");
                $carrito->eliminar();
            }
        }
    }
    
    $datosCli = new Cliente($_SESSION["id"]);
    $datosCli -> consultar();
?>

<div class="container text-center">
	<div class="row mt-3">
		<div class="col">
			<div class="card border-primary mb-3">
				<div class="card-header">
					<h1>GRACIAS POR SU COMPRA</h1>
				</div>
				<div class="card-body">					
					<div class="row justify-content-center">						
						<div class="align-self-center" >		
							<h2><?php echo $datosCli -> getNombre() . " " . $datosCli -> getApellido()?></h2>					
						    <a href="facturaPDF.php" class="btn btn-success" target="_blank">GENERAR FACTURA PDF</a>			
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>