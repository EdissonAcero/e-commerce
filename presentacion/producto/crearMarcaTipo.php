<?php

    if (isset($_POST["crearMarca"])) {
        $marca = new Marca("", $_POST["marca"], $_POST["descripcionMarca"]);
        $marca -> crearMarca();
    } else if (isset($_POST["crearTipo"])) {
        $tipo = new ClaseLicor("", $_POST["tipo"], $_POST["descripcionTipo"]);
        $tipo -> crearTipoLicor();
    }

?>
<body class="fondo-loging">
	<div class="container-sm w-50 bg-primary mt-5 bg-light rounded shadow">

		<div class="row align-items-stretch">

			<section class="col bg-white rounded-end form-box justify-content-center align-items-center col-sm-12">

				<!-- botones login y registro -->
				<div class="butonbox">
					<div id="btn"></div>
					<button type="button" class="toggle-btn" id="btnMarca">Crear Marca</button>
					<button type="button" class="toggle-btn" id="btnTipo">Crear Clase</button>
				</div>

				<div id="CrearMarca">
					<h1 class="text-center">Marca de Licor</h1>
					<!-- formulario Crear Marca -->
					<form method="post" 
						action="index.php?pid=<?php echo base64_encode("presentacion/producto/crearMarcaTipo.php")?>">
						<div class="mb-3">
							<label class="form-label">Nombre Marca</label> <input type="text"
								class="form-control" name="marca">
						</div>
						<div class="mb-3">
							<label class="form-label">Descripcion</label>
							<textarea class="form-control" rows="3" name="descripcionMarca"></textarea>
						</div>
						<div class="d-grid">
							<button type="submit" name="crearMarca" class="btn btn-primary">Crear</button>
						</div>
					</form>
					<!-- fin contenido formulario -->
					<br> <br> <br>
					<!-- errores de ingreso -->
					<?php if ($creado == 1) {/*envio de datos exitoso*/ ?>
						<script>
							var correcto = '<?= $_POST["creado"] ?>';
						</script><!-- asigno variable de error a a variable java script -->
						<div class="alert alert-success alert-dismissible fade show text-center" role="alert">
							<strong>Datos Ingresados</strong>
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
						</div><br>
					<?php } ?>
					<!-- fin errores ingreso -->
				</div>
				
				<div id="CrearTipo">
					<h1 class="text-center">Clase de Licor</h1>
					<!-- formulario Crear Tipo -->
					<form method="post"
						action="index.php?pid=<?php echo base64_encode("presentacion/producto/crearMarcaTipo.php") ?>">
						<div class="mb-3">
							<label class="form-label">Nombre Tipo Licor</label> <input
								type="text" class="form-control" name="tipo">
						</div>
						<div class="mb-3">
							<label class="form-label">Descripcion</label>
							<textarea class="form-control" rows="3" name="descripcionTipo"></textarea>
						</div>

						<div class="d-grid">
							<button type="submit" name="crearTipo" class="btn btn-primary">Crear</button>
						</div>
					</form>
					<!-- fin contenido formulario -->
					<br> <br> <br>
					<!-- errores de registro -->
					<?php if ( $creado == 2) {/*envio de datos exitoso*/ ?>
						<script>
							var correcto = '<?= $_POST["creado"] ?>';
						</script><!-- asigno variable de error a a variable java script -->
						<div class="alert alert-success alert-dismissible fade show text-center" role="alert">
							<strong>Datos Ingresados</strong>
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
						</div><br>
					<?php } ?>
					<!-- fin errores de registro -->
				</div>
			</section>
		</div>
	</div>
	<script>
		//acciones de botones
		let btnMarca = document.getElementById("btnMarca"); //variable almacenar referencia a id del div
		let btnTipo = document.getElementById("btnTipo");

		//funcion auto click
		$(document).ready(function() {
			setTimeout(clickbutton, 2); // indicamos que se ejecuta la función a los 2 segundos de haberse cargado la pagina
			function clickbutton() {
				btnMarca.click(); // simulamos el click del mouse en el boton del formulario login
			}
			//carga los mensajes de alerta por errores de ingreso o registro
		});
		
		//eventos para validar que se haga click
		btnMarca.addEventListener("click", CrearMarcas);
		btnTipo.addEventListener("click", CrearTipos);
		
		//aciones de formularios (variables)
		var x = document.getElementById("CrearMarca");
		var y = document.getElementById("CrearTipo");
		var z = document.getElementById("btn");
		
		//funciones formulariio
		function CrearMarcas() {
			x.style.left = "30px";
			y.style.left = "1000px";
			z.style.left = "0";
		}

		function CrearTipos() {
			x.style.left = "-1000px";
			y.style.left = "30px";
			z.style.left = "100px";
		}
	</script>
</body>
