<?php
$carrito = new carrito();
if (isset($_GET["idproducto"]) && isset($_POST["cantidad"])) {
    $carrito = new carrito("", $_GET["idproducto"], 5, "");
    $existe = $carrito->productoExiste(); // validamos que el cliente no ingrese dos veces el mismo producto
    if ($existe != NULL) {
        $nuevaCantidad = $existe + $_POST["cantidad"];
        $carrito = new carrito("", $_GET["idproducto"], 5, $nuevaCantidad);
        $carrito->updateProducto();
        // ob_start();
        header("Location: index.php?pid=" . base64_encode("presentacion/vendedor/RegistroProducto.php"));
        // ob_end_flush();
    } else {
        $carrito = new carrito("", $_GET["idproducto"], 5, $_POST["cantidad"]);
        $carrito->guardaProductos();
        // ob_start();
        header("Location: index.php?pid=" . base64_encode("presentacion/vendedor/RegistroProducto.php"));
        // ob_end_flush();
    }
}

if (isset($_GET["idEliminar"])) {
    $carrito = new carrito("", $_GET["idEliminar"], 5, "");
    $carrito->eliminar();
}

    date_default_timezone_set('America/Bogota');
    setlocale(LC_ALL, 'es_ES');
    $DiaLetra = date("l");
    $fecha = date('d/m/y');

?>
<div class="container">
	
	<div class="row mt-3">
		<div class="col">
			<div class="card mb-3">
				<div class="card-header">
					<h1>
						PRODUCTOS COMPRAS <small class="tittles-pages-logo"></small>
					</h1>
					<?php echo "fecha ", $DiaLetra, " ", $fecha?>
				</div>
				<div class="card-body">
						<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th width="8%">#</th>
								<th width="15%">Descripcion</th>
								<th width="15%">Foto</th>
								<th width="15%">Precio</th>
								<th width="15%">cantidad</th>
								<th width="15%">Acciones</th>
							</tr>
						</thead>
						<tbody>
    						<?php
                                $sesionVentaCliente = 5;
                                $proCarro = $carrito->listaCarrito($sesionVentaCliente); // traigo todos los datos del carro por id de cliente
                                if ($proCarro != NULL) { // valido que si hay productos
                                    $i = 1;
                                    foreach ($proCarro as $proActual) { // recorro con un foreach
                                        $datoProducto = new Producto($proActual->getIdProducto()); // traigo todos los datos del producto por el id
                                        $datoProducto->consultar(); // consulta
                                        echo "<tr>"; // inicio tabla
                                        echo "<form action='index.php?pid=" . base64_encode("presentacion/carro/actualizarCarrito.php") . "' method='post'>";
                                        echo "<td>" . $i ++ . "</td>";
                                        echo "<td>" . $datoProducto->getIdMarca()->getNombre() . " " . $datoProducto->getIdClaseLicor()->getNombre() . "<br>" . $datoProducto->getTipo() . "</td>";
                                        echo "<td><img src='" . $datoProducto->getFoto() . "' width='30%' ></td>";
                                        echo "<td>" . $datoProducto->getValorUnidad() . "</td>";
                                        echo "<td>" . $proActual->getCantidad() . "</td>";
                                        echo "<td><a href ='index.php?pid=" . base64_encode("presentacion/carrito/generarFactura.php") . "&idEliminar=" . $proActual->getIdProducto() . "' class='btn btn-danger btn-sm'><span class='fas fa-trash'></span></a></td>";
                                        echo "</form>";
                                        echo "</tr>"; // fin tabla
                                    }
                                } else { // si no hay productos
                                    echo "<tr class='text-center'>";
                                    echo "<td colspan='7'> :( NO SE HAN REALIZADO COMPRAS :( </td>";
                                    echo "</tr>";
                                }
                            ?>            	            				
						</tbody>
						<tfoot>
							<tr>
								<th colspan="3" class="text-end">Total</th>
								<th>
    								<?php 
                                        // suma de los productos para total
                                        $suma = 0; // declaro una variabla para almacenar la suma
                                        foreach ($proCarro as $proActual) {
                                            $datoProducto = new Producto($proActual->getIdProducto());
                                            $datoProducto->consultar(); // vuelvo a consultar
                                            $suma += $proActual->getCantidad() * $datoProducto->getValorUnidad(); // multiplico la cantidad por el valor y sumo el resultado en cada iteracion
                                        }
                                        echo $suma; // muestro la suma o total
                                      ?>
                                 </th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
					</table>
					<?php 
					if ($proCarro != NULL) {
					    echo "<form action='index.php?pid=" . base64_encode("presentacion/vendedor/finalizarVenta.php") . "' method='post'>";
					    echo "<div class='mb-3 form-check'>";
					     "<input disabled type='checkbox' class='form-check-input' id='domicilio' name='domicilio'>";
					     "<label class='form-check-label'>Envio a domicilio</label>";
					    echo "</div>";
					    echo "<button class='btn btn-success btn-block' type='submit'>FINALIZAR VENTA</button>";
					    echo "<form>";
					} 
                    ?>
				</div>
			</div>
		</div>
	</div>
</div>



